class LatestIssuesWidget < BaseWidget

  def issues
    Issue.joins(:issue_state)
        .includes(:issue_state)
        .where('issue_states.completed = 0')
        .order(created_at: :desc)
        .limit(10)
  end

end
