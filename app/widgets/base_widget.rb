class BaseWidget

  def template_file
    template_file = self.class.name.delete_suffix('Widget').tableize

    "widgets/#{template_file}"
  end

end
