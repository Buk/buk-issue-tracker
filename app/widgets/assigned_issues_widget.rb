class AssignedIssuesWidget < BaseWidget

  def initialize(user)
    @current_user = user
  end

  def issues
    Issue.joins(:issue_state)
        .includes(:issue_state)
        .where('issue_states.completed = 0')
        .where(assigned_user_id: @current_user)
        .order(created_at: :desc)
        .limit(10)
  end

end
