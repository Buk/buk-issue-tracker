class IssuesController < ApplicationController
  before_action :set_issue, only: [:show, :edit, :update, :destroy]
  before_action :load_form_options, only: [:new, :show, :create]

  # GET /issues
  # GET /issues.json
  def index
    @issues_grid = IssueGrid.new

    if !params[:search] != nil
      @search_input_value = params[:search]
      @issues_grid.search_for(params[:search])
    end

    @issues_grid.set_page(params[:page] || 1)

    @issues = @issues_grid.get_results
  end

  # GET /issues/1
  # GET /issues/1.json
  def show
    @issue_comments = @issue.issue_comments.order(:created_at).all

    @issue_comment = IssueComment.new
    @issue_comment.issue = @issue
    @issue_comment.user = current_user
  end

  # GET /issues/new
  def new
    @issue = Issue.new
    @issue.project = Project.find(params[:project_id]) if params[:project_id]
    @issue.issue_state = IssueState.where(completed: false).first
  end

  # GET /issues/1/edit
  def edit
    redirect_to @issue
  end

  # POST /issues
  # POST /issues.json
  def create
    @issue = Issue.new(issue_params)
    @issue.issue_type = IssueType.find(issue_params[:issue_type_id])
    @issue.issue_priority = IssuePriority.find(issue_params[:issue_priority_id])
    @issue.issue_state = IssueState.find(issue_params[:issue_state_id])
    @issue.assigned_user = issue_params[:assigned_user_id] != nil ? User.find(issue_params[:assigned_user_id]) : nil

    respond_to do |format|
      if @issue.save
        format.html { redirect_to @issue, notice: 'Issue was successfully created.' }
        format.json { render :show, status: :created, location: @issue }
      else
        format.html { render :new }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /issues/1
  # PATCH/PUT /issues/1.json
  def update
    respond_to do |format|
      if @issue.update(issue_params)
        format.html { redirect_to @issue, notice: 'Issue was successfully updated.' }
        format.json { render :show, status: :ok, location: @issue }
      else
        format.html { render :edit }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /issues/1
  # DELETE /issues/1.json
  def destroy
    @issue.destroy
    respond_to do |format|
      format.html { redirect_to issues_url, notice: 'Issue was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_issue
      begin
        @issue = Issue.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        flash[:error] = "Issue not found"
        redirect_back(fallback_location: issues_path)
      end
    end

    # Only allow a list of trusted parameters through.
    def issue_params
      params.require(:issue).permit(:name, :type, :priority, :description, :state, :project_id, :issue_type_id, :issue_priority_id, :issue_state_id, :assigned_user_id)
    end

    def load_form_options
      @available_projects = Project.order(:name).pluck(:name, :id)
      @available_issue_types = IssueType.pluck(:name, :id)
      @available_issue_priorities = IssuePriority.order(value: :desc).pluck(:name, :id)
      @available_issue_states = IssueState.order(:completed).pluck(:name, :id)
      @available_users = User.order(:email).pluck(:email, :id)
    end
end
