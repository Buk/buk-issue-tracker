json.extract! issue, :id, :name, :issue_type, :issue_priority, :description, :issue_state, :created_at, :updated_at
json.url issue_url(issue, format: :json)
