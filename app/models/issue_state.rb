class IssueState < ApplicationRecord

  validates :name, :color, :completed, presence: true

  has_many :issues

end
