class IssueType < ApplicationRecord

  validates :name, :icon, :color, presence: true

  has_many :issues

end
