class Project < ApplicationRecord

  has_many :issues, dependent: :destroy

  validates :name, :project_key, presence: true

  before_save :normalize_project_key

  def issues_total
    (@issues_count ||= fetch_issue_count)[:total]
  end

  def issues_open
    (@issues_count ||= fetch_issue_count)[:open]
  end

  def issues_closed
    (@issues_count ||= fetch_issue_count)[:closed]
  end

  def create_key_for_new_issue
    new_issue_key = "#{self.project_key}-#{self.next_issue_key}"

    self.update_attribute :next_issue_key, (self.next_issue_key + 1)

    new_issue_key
  end

  private
  def normalize_project_key
    self.project_key = self.project_key.upcase
  end

  def fetch_issue_count
    @issues_count = self.issues.joins(:issue_state).group(:completed).count
    @issues_count.default=0

    {
        open: @issues_count[0],
        closed: @issues_count[1],
        total: @issues_count[0] + @issues_count[1],
    }
  end

end
