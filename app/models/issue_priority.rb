class IssuePriority < ApplicationRecord

  validates :name, :icon, :color, :value, presence: true

  has_many :issues

end
