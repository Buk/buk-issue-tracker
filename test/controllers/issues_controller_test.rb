require 'test_helper'

class IssuesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    sign_in users(:user1)
    @issue = issues(:issue_assigned)
  end

  test "should get index" do
    get issues_url
    assert_response :success
  end

  test "should get new" do
    get new_issue_url
    assert_response :success
  end

  test "should create issue" do
    assert_difference('Issue.count') do
      issue = {
          description: @issue.description,
          name: @issue.name,
          project_id: @issue.project_id,
          issue_priority_id: @issue.issue_priority_id,
          issue_state_id: @issue.issue_state_id,
          issue_type_id: @issue.issue_type_id,
      }

      post issues_url, params: { issue: issue }
    end

    assert_redirected_to issue_url(Issue.last)
  end

  test "should show issue" do
    get issue_url(@issue)
    assert_response :success
  end

  test "should get edit" do
    get edit_issue_url(@issue)
    assert_redirected_to issue_url(@issue)
  end

  test "should update issue" do
    issue = {
        description: @issue.description,
        name: @issue.name,
        project_id: @issue.project_id,
        issue_priority_id: @issue.issue_priority_id,
        issue_state_id: @issue.issue_state_id,
        issue_type_id: @issue.issue_type_id,
        assigned_user_id: @issue.assigned_user_id,
    }

    patch issue_url(@issue), params: { issue: issue }
    assert_redirected_to issue_url(@issue)
  end

  test "should destroy issue" do
    assert_difference('Issue.count', -1) do
      delete issue_url(@issue)
    end

    assert_redirected_to issues_url
  end
end
