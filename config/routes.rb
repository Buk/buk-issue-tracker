Rails.application.routes.draw do
  resources :projects
  resources :issues do
    resources :issue_comments, only: [:show, :create, :update, :destroy]
  end


  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout'}

  root 'home#index'
end
