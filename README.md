# Buk issue tracking
 
 Made as a sample project to learn Ruby on Rails

* Users have to be signed in to access data
* Projects have many issues
* Issue has type, priority, state and can be assigned to a user
* Issues can have comments

Dummy data are available via `rails db:seed`