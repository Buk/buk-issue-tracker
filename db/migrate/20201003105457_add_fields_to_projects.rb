class AddFieldsToProjects < ActiveRecord::Migration[6.0]
  def change
    change_table :projects do |t|
      t.string :project_key
      t.integer :next_issue_key, default: 1

      t.index :project_key, { unique: true }
    end

    change_table :issues do |t|
      t.string :issue_key

      t.index :issue_key, { unique: true }
    end
  end
end
