priorities = IssuePriority.create!([
                                      {
                                          name: 'Low',
                                          icon: 'chevron-down',
                                          color: '#9CCC65',
                                          value: 0,
                                      },
                                      {
                                          name: 'Medium',
                                          icon: 'chevron-up',
                                          color: '#FB8C00',
                                          value: 5,
                                      },
                                      {
                                          name: 'High',
                                          icon: 'chevron-double-up',
                                          color: '#FB8C00',
                                          value: 10,
                                      },
                                      {
                                          name: 'ASAP',
                                          icon: 'exclamation-square',
                                          color: '#E53935',
                                          value: 50,
                                      }
                                  ])

types = IssueType.create!([
                             {
                                 name: 'Bug',
                                 icon: 'bug',
                                 color: '#d32f2f',
                             },
                             {
                                 name: 'Feature',
                                 icon: 'sparkles',
                                 color: '#1976D2',
                             },
                         ])


states = IssueState.create!([
                               {
                                   name: 'New',
                                   color: '#303F9F',
                                   completed: false,
                               },
                               {
                                   name: 'In Progress',
                                   color: '#FFA000',
                                   completed: false,
                               },
                               {
                                   name: 'Done',
                                   color: '#388E3C',
                                   completed: true,
                               },
                           ])

project = Project.create!({
                              name: 'Buk issue tracker',
                              project_key: 'BIT',
                          })

user = User.create!({
                        email: 'jan@bukva.cz',
                        password: 'changeme',
                    })

for i in 1 .. 42
  assigned_user = rand(3) == 2 ? user : nil

  issue = Issue.create!({
                            name: Faker::Hacker.say_something_smart,
                            description: Faker::Lorem.paragraph,
                            issue_type: types.shuffle[0],
                            issue_state: states.shuffle[0],
                            issue_priority: priorities.shuffle[0],
                            project: project,
                            assigned_user: assigned_user,
                        })

  commentsCount = rand(6)

  for i in 1 .. commentsCount
    IssueComment.create!([
                            comment: Faker::Hacker.say_something_smart,
                            user: user,
                            issue: issue,
                        ])
  end
end
